###############################################################################
# Required Variables
###############################################################################

variable "url" {
  type        = string
  description = "The URL of the backend."
}

variable "name" {
  type        = string
  description = "The full name of the API Management backend."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management backend."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this backend should be created."
}

###############################################################################
# Optional Variables
###############################################################################

variable "protocol" {
  type        = string
  default     = "http"
  description = "The protocol used by the backend host. Can be 'http' or 'soap'."
}

###############################################################################

variable "credentials_headers" {
  type        = map(string)
  default     = null
  description = "A map of extra headers to present to the backend host."
}

variable "credentials_certificate_thumbprints" {
  type        = list(string)
  default     = null
  description = "A list of client certificate thumbprints to present to the backend host. The certificates must exist within the API Management Service."
}

###############################################################################
# Locals
###############################################################################

locals {
  credentials_enabled = var.credentials_headers != null || var.credentials_certificate_thumbprints != null
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_backend" "object" {
  url                 = var.url
  name                = var.name
  protocol            = var.protocol
  api_management_name = var.service
  resource_group_name = var.group

  dynamic "credentials" {
    for_each = local.credentials_enabled ? [{
      "headers"                 = var.credentials_headers
      "certificate_thumbprints" = var.credentials_certificate_thumbprints
    }] : []

    content {
      header      = credentials.value.headers
      certificate = credentials.value.certificate_thumbprints
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "protocol" {
  value = var.protocol
}

###############################################################################

output "credentials_enabled" {
  value = local.credentials_enabled
}

output "credentials_headers" {
  value = var.credentials_headers
}

output "credentials_certificate_thumbprints" {
  value     = var.credentials_certificate_thumbprints
  sensitive = true
}

###############################################################################

output "id" {
  value = azurerm_api_management_backend.object.id
}

output "url" {
  value = azurerm_api_management_backend.object.url
}

output "name" {
  value = azurerm_api_management_backend.object.name
}

###############################################################################
