###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the API Management subscription."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management subscription."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this subscription should be created."
}

###############################################################################
# Optional Variables
###############################################################################

variable "user" {
  type        = string
  default     = null
  description = "User ID this subscription should be attached to. Ignored if 'product' is set."
}

variable "state" {
  type        = string
  default     = "submitted"
  description = "The state of the API management subscription."

  validation {
    condition     = contains([ "active", "cancelled", "expired", "rejected", "submitted", "suspended" ], var.state)
    error_message = "Subscription state must be: active, cancelled, expired, rejected, submitted or suspended."
  }
}

variable "title" {
  type        = string
  default     = null
  description = "The display name of the API management subscription."
}

variable "product" {
  type        = string
  default     = null
  description = "Product ID this subscription should be attached to."
}

###############################################################################
# Locals
###############################################################################

locals {
  user  = var.product == null ? var.user : null
  title = coalesce(var.title, format("%s Subscription", var.name))
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_subscription" "object" {
  state               = var.state
  user_id             = local.user
  product_id          = var.product
  display_name        = local.title
  subscription_id     = var.name
  api_management_name = var.service
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "user" {
  value = local.user
}

output "state" {
  value = var.state
}

output "product" {
  value = var.product
}

###############################################################################

output "id" {
  value = azurerm_api_management_subscription.object.id
}

output "name" {
  value = azurerm_api_management_subscription.object.subscription_id
}

output "title" {
  value = azurerm_api_management_subscription.object.display_name
}

output "key_primary" {
  value     = azurerm_api_management_subscription.object.primary_key
  sensitive = true
}

output "key_secondary" {
  value     = azurerm_api_management_subscription.object.secondary_key
  sensitive = true
}

###############################################################################
