###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the API Management API."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management API."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this API should be created."
}

###############################################################################
# Optional Variables
###############################################################################

variable "path" {
  type        = string
  default     = null
  description = "The Path for this API Management API. Defaults to 'name' if not specified."
}

variable "title" {
  type        = string
  default     = null
  description = "The display name of the API."
}

variable "backend" {
  type        = string
  default     = null
  description = "Absolute URL of the backend service implementing this API."
}

variable "revision" {
  type        = number
  default     = 1
  description = "The Revision which used for this API."
}

variable "protocols" {
  type        = list(string)
  default     = [ "https" ]
  description = "A list of protocols the operations in this API can be invoked. Possible values are 'http' and 'https'."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the API Management API, which may include HTML formatting tags."
}

###############################################################################

variable "subscription_query" {
  type        = string
  default     = "api-access-key"
  description = "The name of the QueryString parameter which should be used for the Subscription Key."
}

variable "subscription_header" {
  type        = string
  default     = "X-API-Access-Key"
  description = "The name of the HTTP Header which should be used for the Subscription Key."
}

variable "subscription_enabled" {
  type        = bool
  default     = true
  description = "Should this API require a subscription key?"
}

###############################################################################
# Locals
###############################################################################

locals {
  path  = coalesce(var.path, var.name)
  title = coalesce(var.title, format("%s API", var.name))
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_api" "object" {
  name                  = var.name
  path                  = local.path
  revision              = var.revision
  protocols             = var.protocols
  description           = var.description
  service_url           = var.backend
  display_name          = local.title
  api_management_name   = var.service
  resource_group_name   = var.group
  subscription_required = var.subscription_enabled

  subscription_key_parameter_names {
    query  = var.subscription_query
    header = var.subscription_header
  }
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "path" {
  value = local.path
}

output "backend" {
  value = var.backend
}

output "revision" {
  value = var.revision
}

output "protocols" {
  value = var.protocols
}

output "description" {
  value = var.description
}

###############################################################################

output "subscription_query" {
  value = var.subscription_query
}

output "subscription_header" {
  value = var.subscription_header
}

output "subscription_enabled" {
  value = var.subscription_enabled
}

###############################################################################

output "id" {
  value = azurerm_api_management_api.object.id
}

output "set" {
  value = azurerm_api_management_api.object.version_set_id
}

output "name" {
  value = azurerm_api_management_api.object.name
}

output "title" {
  value = azurerm_api_management_api.object.display_name
}

output "online" {
  value = azurerm_api_management_api.object.is_online
}

output "current" {
  value = azurerm_api_management_api.object.is_current
}

output "release" {
  value = azurerm_api_management_api.object.version
}

###############################################################################
