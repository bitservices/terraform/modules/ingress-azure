<!---------------------------------------------------------------------------->

# apim/product

#### Manage [API Management] products

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/azure//apim/product`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_service" {
  source   = "gitlab.com/bitservices/ingress/azure//apim/service"
  class    = "api"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_product" {
  source  = "gitlab.com/bitservices/ingress/azure//apim/product"
  name    = "my-product"
  group   = module.my_resource_group.name
  service = module.my_apim_service.name
}
```

<!---------------------------------------------------------------------------->

[API Management]: https://azure.microsoft.com/services/api-management/

<!---------------------------------------------------------------------------->
