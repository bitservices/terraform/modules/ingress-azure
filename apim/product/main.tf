###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the API Management product."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management product."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this product should be created."
}

###############################################################################
# Optional Variables
###############################################################################

variable "title" {
  type        = string
  default     = null
  description = "The display name of the API management product."
}

variable "published" {
  type        = bool
  default     = true
  description = "Should the API management product be marked as published."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the API Management product, which may include HTML formatting tags."
}

###############################################################################

variable "require_approval" {
  type        = bool
  default     = false
  description = "Should new subscriptions to the API management product require approval? Ignored if 'require_subscription' is 'false'."
}

variable "require_subscription" {
  type        = bool
  default     = true
  description = "Is a subscription required to access APIs within this API management product?"
}

###############################################################################
# Locals
###############################################################################

locals {
  title            = coalesce(var.title, format("%s Product", var.name))
  require_approval = var.require_subscription && var.require_approval
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_product" "object" {
  published             = var.published
  product_id            = var.name
  description           = var.description
  display_name          = local.title
  approval_required     = local.require_approval
  api_management_name   = var.service
  resource_group_name   = var.group
  subscription_required = var.require_subscription
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "published" {
  value = var.published
}

output "description" {
  value = var.description
}

###############################################################################

output "require_approval" {
  value = local.require_approval
}

output "require_subscription" {
  value = var.require_subscription
}

###############################################################################

output "id" {
  value = azurerm_api_management_product.object.id
}

output "name" {
  value = azurerm_api_management_product.object.product_id
}

output "title" {
  value = azurerm_api_management_product.object.display_name
}

###############################################################################
