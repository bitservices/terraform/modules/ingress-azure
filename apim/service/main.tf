###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The API Management Service identifier. This is suffixed to the resource group name and must be globally unique."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management Service."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this resource group."
}

###############################################################################
# Optional Variables
###############################################################################

variable "http2" {
  type        = bool
  default     = true
  description = "Should HTTP/2 be supported by the API Management Service."
}

###############################################################################

variable "publisher_name" {
  type        = string
  default     = null
  description = "The name of publisher/company. Defaults to 'company'."
}

variable "publisher_email" {
  type        = string
  default     = null
  description = "The email address of publisher/company. Defaults to 'owner'."
}

###############################################################################

variable "sku_name" {
  type        = string
  default     = "Developer"
  description = "Which SKU type to provision, valid values are: 'Consumption', 'Developer', 'Basic', 'Standard' and 'Premium'."
}

variable "sku_units" {
  type        = number
  default     = 1
  description = "Number of units of 'sku_name' to provision."
}

###############################################################################
# Locals
###############################################################################

locals {
  sku             = format("%s_%d", var.sku_name, var.sku_units)
  name            = format("%s-%s", var.group, var.class)
  publisher_name  = coalesce(var.publisher_name, var.company)
  publisher_email = coalesce(var.publisher_email, var.owner)
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management" "object" {
  name                = local.name
  sku_name            = local.sku
  location            = var.location
  publisher_name      = local.publisher_name
  publisher_email     = local.publisher_email
  resource_group_name = var.group

  tags = {
    "SKU"          = local.sku
    "Name"         = local.name
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  protocols {
    enable_http2 = var.http2
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "http2" {
  value = var.http2
}

###############################################################################

output "publisher_name" {
  value = local.publisher_name
}

output "publisher_email" {
  value = local.publisher_email
}

###############################################################################

output "sku" {
  value = local.sku
}

output "sku_name" {
  value = var.sku_name
}

output "sku_units" {
  value = var.sku_units
}

###############################################################################

output "id" {
  value = azurerm_api_management.object.id
}

output "scm" {
  value = azurerm_api_management.object.scm_url
}

output "name" {
  value = azurerm_api_management.object.name
}

output "tenant" {
  value = azurerm_api_management.object.tenant_access
}

output "gateway" {
  value = azurerm_api_management.object.gateway_url
}

output "identity" {
  value = azurerm_api_management.object.identity
}

output "management" {
  value = azurerm_api_management.object.management_api_url
}

output "ipv4_public" {
  value = azurerm_api_management.object.public_ip_addresses
}

output "ipv4_private" {
  value = azurerm_api_management.object.private_ip_addresses
}

output "location_extra" {
  value = azurerm_api_management.object.additional_location
}

output "gateway_regional" {
  value = azurerm_api_management.object.gateway_regional_url
}

output "portal_developer" {
  value = azurerm_api_management.object.developer_portal_url
}

output "portal_publisher" {
  value = azurerm_api_management.object.portal_url
}

###############################################################################
