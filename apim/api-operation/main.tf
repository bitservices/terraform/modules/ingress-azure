###############################################################################
# Required Variables
###############################################################################

variable "api" {
  type        = string
  description = "The Name of the API Management API where this Operation should be created."
}

variable "name" {
  type        = string
  description = "The full name of the API Management API Operation."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management API."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this API should be created."
}

###############################################################################
# Optional Variables
###############################################################################

variable "title" {
  type        = string
  default     = null
  description = "The display name of the API."
}

variable "method" {
  type        = string
  default     = "GET"
  description = "The HTTP Method used for this API Management API Operation."
}

variable "template" {
  type        = string
  default     = null
  description = "The relative URL Template identifying the target resource for this operation, which may include parameters."
}

variable "description" {
  type        = string
  default     = "Managed by Terraform"
  description = "A description of the API Management API Operation, which may include HTML formatting tags."
}

###############################################################################
# Locals
###############################################################################

locals {
  title    = coalesce(var.title, format("%s API", var.name))
  template = coalesce(var.template, format("/%s", var.name))
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_api_operation" "object" {
  method              = var.method
  api_name            = var.api
  description         = var.description
  display_name        = local.title
  operation_id        = var.name
  url_template        = local.template
  api_management_name = var.service
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "api" {
  value = var.api
}

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "method" {
  value = var.method
}

output "template" {
  value = local.template
}

output "description" {
  value = var.description
}

###############################################################################

output "id" {
  value = azurerm_api_management_api_operation.object.id
}

output "name" {
  value = azurerm_api_management_api_operation.object.operation_id
}

output "title" {
  value = azurerm_api_management_api_operation.object.display_name
}

###############################################################################
