<!---------------------------------------------------------------------------->

# apim/api-operation

#### Manage [API Management] API operations

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/azure//apim/api-operation`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_service" {
  source   = "gitlab.com/bitservices/ingress/azure//apim/service"
  class    = "api"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_api" {
  source  = "gitlab.com/bitservices/ingress/azure//apim/api"
  name    = "users-api"
  group   = module.my_resource_group.name
  service = module.my_apim_service.name
}

module "my_apim_api_operation" {
  source  = "gitlab.com/bitservices/ingress/azure//apim/api-operation"
  api     = module.my_apim_api.name
  name    = "get-user"
  group   = module.my_resource_group.name
  service = module.my_apim_service.name
}
```

<!---------------------------------------------------------------------------->

[API Management]: https://azure.microsoft.com/services/api-management/

<!---------------------------------------------------------------------------->
