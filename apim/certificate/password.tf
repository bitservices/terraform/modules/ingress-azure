###############################################################################
# Resource - random_password
###############################################################################

resource "random_password" "object" {
  length  = 64
  special = false

  keepers = {
    "api_management_certificate_key"     = sensitive(sha256(var.key))
    "api_management_certificate_cert"    = sensitive(sha256(var.cert))
    "api_management_certificate_name"    = var.name
    "api_management_certificate_group"   = var.group
    "api_management_certificate_service" = var.service
    "api_management_certificate_service" = var.service
  }
}

###############################################################################
# Outputs
###############################################################################

output "password" {
  value     = random_password.object.result
  sensitive = true
}

###############################################################################
