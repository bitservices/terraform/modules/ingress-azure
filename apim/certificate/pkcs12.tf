###############################################################################
# Resources
###############################################################################

resource "pkcs12_from_pem" "object" {
  cert_pem        = var.cert
  password        = random_password.object.result
  private_key_pem = var.key
}

###############################################################################
# Outputs
###############################################################################

output "pkcs12" {
  value     = pkcs12_from_pem.object.result
  sensitive = true
}

###############################################################################
