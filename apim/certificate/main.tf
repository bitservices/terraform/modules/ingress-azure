###############################################################################
# Required Variables
###############################################################################

variable "key" {
  type        = string
  description = "The private key in PEM format."
}

variable "cert" {
  type        = string
  description = "The certificate in PEM format."
}

variable "name" {
  type        = string
  description = "The full name of the API Management certificate."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this API Management certificate."
}

variable "service" {
  type        = string
  description = "The Name of the API Management Service where this certificate should be created."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_api_management_certificate" "object" {
  name                = var.name
  data                = pkcs12_from_pem.object.result
  password            = random_password.object.result
  api_management_name = var.service
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "key" {
  value     = var.key
  sensitive = true
}

output "cert" {
  value     = var.cert
  sensitive = true
}

output "group" {
  value = var.group
}

output "service" {
  value = var.service
}

###############################################################################

output "id" {
  value = azurerm_api_management_certificate.object.id
}

output "name" {
  value = azurerm_api_management_certificate.object.name
}

output "expires" {
  value = azurerm_api_management_certificate.object.expiration
}

output "subject" {
  value = azurerm_api_management_certificate.object.subject
}

output "thumbprint" {
  value = azurerm_api_management_certificate.object.thumbprint
}

###############################################################################
