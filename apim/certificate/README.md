<!---------------------------------------------------------------------------->

# apim/certificate

#### Manage [API Management] certificates

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/ingress/azure//apim/certificate`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

resource "tls_private_key" "my_tls_key" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_self_signed_cert" "my_tls_cert" {
  private_key_pem       = tls_private_key.my_tls_key.private_key_pem
  early_renewal_hours   = 8760
  validity_period_hours = 43800

  allowed_uses = [
    "client_auth",
    "key_encipherment",
    "digital_signature"
  ]

  subject {
    common_name         = "foobar-api"
    organization        = var.company
    organizational_unit = "foobar"
  }
}

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_service" {
  source   = "gitlab.com/bitservices/ingress/azure//apim/service"
  class    = "api"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_apim_certificate" {
  source  = "gitlab.com/bitservices/ingress/azure//apim/certificate"
  key     = tls_private_key.my_tls_key.private_key_pem
  cert    = tls_self_signed_cert.my_tls_cert.cert_pem
  name    = "tls"
  group   = module.my_resource_group.name
  service = module.my_apim_service.name
}
```

<!---------------------------------------------------------------------------->

[API Management]: https://azure.microsoft.com/services/api-management/

<!---------------------------------------------------------------------------->
