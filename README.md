<!---------------------------------------------------------------------------->

# ingress (azure)

<!---------------------------------------------------------------------------->

## Description

Manages ingress components for [Azure] such as [Load Balancing] and
[API Management].

<!---------------------------------------------------------------------------->

## Modules

* [apim/api](apim/api/README.md) - Manage [API Management] API instances.
* [apim/api-operation](apim/api-operation/README.md) - Manage [API Management] API operations.
* [apim/backend](apim/backend/README.md) - Manage [API Management] backends.
* [apim/certificate](apim/certificate/README.md) - Manage [API Management] certificates.
* [apim/product](apim/product/README.md) - Manage [API Management] products.
* [apim/service](apim/service/README.md) - Manage [API Management] service instances.
* [apim/subscription](apim/subscription/README.md) - Manage [API Management] subscriptions.

<!---------------------------------------------------------------------------->

[Azure]:          https://azure.microsoft.com/
[API Management]: https://azure.microsoft.com/services/api-management/
[Load Balancing]: https://azure.microsoft.com/products/azure-load-balancing/

<!---------------------------------------------------------------------------->
